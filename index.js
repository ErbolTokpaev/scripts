const { Client, logger } = require('camunda-external-task-client-js');
const BearerTokenInterceptor = require('camunda-external-task-bearer-token-interceptor');
const { Variables } = require("camunda-external-task-client-js");


// ################ Keycloak ##########################
let bearerTokenInterceptor = null;
if (process.env.KEYCLOAK_CLIENT_ID && process.env.KEYCLOAK_CLIENT_SECRET && process.env.KEYCLOAK_BASE_URL && process.env.KEYCLOAK_REALM_ID) {
  bearerTokenInterceptor = new BearerTokenInterceptor({
    clientId: process.env.KEYCLOAK_CLIENT_ID,
    clientSecret: process.env.KEYCLOAK_CLIENT_SECRET,
    baseUrl: process.env.KEYCLOAK_BASE_URL,
    readlmId: process.env.KEYCLOAK_REALM_ID,
  });
}
// ################ Camunda ##########################
const config = { baseUrl: process.env.CAMUNDA_URL, use: logger, interceptors: bearerTokenInterceptor, workerId: 'workflow-messages', interval: 1000 };

// create a Client instance with custom configuration
const client = new Client(config);

// Присвоение номера НД
client.subscribe('set-permit-num', async function({ task, taskService }) {
  // Put your business logic here
  var GoogleSpreadsheet = require('google-spreadsheet');
  var doc = new GoogleSpreadsheet('1ErEZ5jN1WSam3UD2Ef1mqTQc1b61qWBxyVYfiTRxrns');
  var creds = require('./client_secret_hwp.json');
  var async = require('async');
  var sheet;
  var x ;
  async.series([
    function setAuth(step) {
      doc.useServiceAccountAuth(creds, step);
    },
    function getInfoAndWorksheets(step) {
      doc.getInfo(function(err, info) {
        var permitKind = task.variables.getAllTyped().kindOfWork.value
        if (permitKind == 'commission'){
          sheet = info.worksheets[0];
        }
        if (permitKind == 'construction') {
          sheet = info.worksheets[1];
        }
        step();
      });
    },
    function workingWithRows(step) {
      sheet.getRows({
        offset: 1,
      }, function( err, rows ){
        var new_permit_id_str = '000001'
        if (rows) {
          if (rows.length > 0) {
            var cur_permit_id = rows[rows.length-1].permitid
            var new_permit_id = + cur_permit_id + 1

            new_permit_id_str = new_permit_id.toString()
            var str = '000000' + new_permit_id_str

            new_permit_id_str = str.slice(new_permit_id_str.length)
          }
          
        }
        const variables = new Variables().setAllTyped({
          permitId: {
            value: new_permit_id_str,
            type: 'String'
          },
        });
        taskService.complete(task, variables).then(result => {
          console.log(`Result: ${result}`);
        })
      });
    },    
    ], 
    function(err){
       if( err ) {
          console.log('Error: '+err);
       }
     })
});
  // запись текущего номера НД в таблицу
client.subscribe('update-permit-table', async function({ task, taskService }) {
  // Put your business logic here
  var GoogleSpreadsheet = require('google-spreadsheet');
  var doc = new GoogleSpreadsheet('1ErEZ5jN1WSam3UD2Ef1mqTQc1b61qWBxyVYfiTRxrns');
  var creds = require('./client_secret_hwp.json');
  var async = require('async');
  var sheet;
  var x ;
  var date = new Date()
  obj = {
    permitid: task.variables.getAllTyped().permitId.value,
    Initiator: task.variables.getAllTyped().initiator.value,
    created: date,
    createdby: '',
  },
  async.series([    
    function setAuth(step) {
      doc.useServiceAccountAuth(creds, step);
    },
    function getInfoAndWorksheets(step) {
      doc.getInfo(function(err, info) {
        console.log('Loaded doc: '+info.title+' by '+info.author.email);
        var permitKind = task.variables.getAllTyped().kindOfWork.value
        console.log(task.variables.getAllTyped().kindOfWork.value)
        if (permitKind == 'commission'){
          sheet = info.worksheets[0];
        }
        if (permitKind == 'construction') {
          sheet = info.worksheets[1];
        }
        step();
      });
    },
    
    function addToRow(step) {
      sheet.addRow(obj, function (err, rows){
        taskService.complete(task).then(() => {
          console.log(`Task completed: ${task.id}`);
        });
      })
    }
    ], 
    function(err){
       if( err ) {
          console.log('Error: '+err);
       }
     })

    

});
